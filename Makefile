SOURCES = $(shell find src/main -type f -name '*.java')
all: build

build:
	mkdir -p bin
	javac $(SOURCES) -d bin

run: build
	java -cp bin/ casir.lifegame.LifeGame

