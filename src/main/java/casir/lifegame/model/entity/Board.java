package casir.lifegame.model.entity;

public class Board {

    public boolean[][] content;

    public Board(int i){
        this.content = new boolean[i][i];
        boolean[] line;
        for (int x=0; x<i; x++){
            line = new boolean[i];
            for (int y=0; y<i; y++) {
                line[y] = false;
            }
            this.content[x] = line;
        }
    }

    public boolean equal(Board board) {
        for (int x=0; x<this.content.length; x++){
            for (int y=0; y<this.content.length; y++) {
                if (this.content[x][y] != board.content[x][y]){
                    return false;
                }
            }
        }
        return true;
    }

    public Board clone(){
        Board clone = new Board(this.content.length);
        for (int x=0; x<this.content.length; x++){
            for (int y=0; y<this.content.length; y++) {
                clone.content[x][y] = this.content[x][y];
            }
        }
        return clone;
    }
}