package casir.lifegame;

import casir.lifegame.model.entity.Board;

public class LifeGame {

    Board board1;
    Board board2;

    public static void main(String[] args){
        LifeGame game = new LifeGame();
    }

    public LifeGame(){
        this.board1 = new Board(20);
        this.board2 = new Board(20);
        this.board1.content[17][17] = true;
        this.board1.content[18][17] = true;
        this.board1.content[17][18] = true;
        this.board1.content[19][18] = true;
        this.board1.content[17][19] = true;

        this.start();

    }

    public void start(){

        while(!this.board1.equal(this.board2)){
            UserInterface.displayGameBoard(this.board1);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            this.board2 =  this.board1.clone();
            this.board1 = new Board(20);

            for (int x=0; x<20; x++){
                for (int y=0; y<20; y++) {
                    int number = 0;
                    for (int v= -1; v <= 1; v++){
                        for (int h = -1; h <= 1; h++){
                            if (!(v== 0 && h== 0)){
                                if (x+h >= 0 && x+h < 20 && y + v >= 0 && y+v < 20){
                                    boolean result =  this.board2.content[x+h][y+v];
                                    if (result){
                                        number ++;
                                    }
                                }
                            }
                        }
                    }
                    this.defineBoardContent(x, y, number);
                }
            }
        }
        UserInterface.displayEndMessage();
    }

    public void defineBoardContent(int x, int y, int nb){
        if ( this.board2.content[x][y]){
            if (nb < 2 || nb > 3){
                this.board1.content[x][y] = false;
            } else {
                this.board1.content[x][y] = true;
            }
        } else {
            if (nb == 3){
                this.board1.content[x][y] = true;
            } else {
                this.board1.content[x][y] = false;
            }
        }
    }
}